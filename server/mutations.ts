import { Token } from '../models/Token';
import { MongoDB } from '../database/Mongo';
import * as bcrypt from 'bcrypt';

class Credentials {
  username: string;
  password: string;
}

const SALT = 10;

export const mutations = function(db: MongoDB) {
  return {
    getToken(_: any, credentials: Credentials) {
      return new Promise((resolve, reject) => {
        const result = db.user.findOne({ username: credentials.username }, (err, user: any) => {
          if (err || !user) {
             reject("User not found");
          } else {
            bcrypt.compare(credentials.password, user.password, (err, passCheck) => {
              if (passCheck) {
                let token = new Token();
                user.update({ $set: { token: token } }, (err, res) => {
                  db.user.findOne({username: credentials.username}, (erro, userData) => {
                    return resolve(userData.token);
                  });
                });
              } else {
                reject("Wrong password");
              }
            });
          }
        });
      });
    }
  };
};
