'use strict';
import * as express from 'express';
import * as http from 'http';
import {Request, Response} from 'express';
import {text, ParsedAsText} from 'body-parser';
import * as session from 'express-session';
import * as gql from 'express-graphql';
import {
  graphql,
  GraphQLSchema,
  GraphQLObjectType,
  GraphQLString,
  GraphQLList
} from 'graphql';
import { MongoDB } from '../database/Mongo';
import { schema } from './resolvers';


export const server = function(port: number, db :MongoDB) {
  const app = express();

  app.use(session({
    secret: 'TwojaStaraOfDeath',
    resave: false,
    saveUninitialized: true,
    cookie: { maxAge: 60000 }})
  );

  app.use(text({ type: 'application/graphql' }));

  app.post('/graphql', (req: Request & ParsedAsText, res: Response) => {
    return graphql(schema(db), req.body)
    .then(result => {
      res.send(JSON.stringify(result, null, 2));
    });
  });

  return app.listen(port);
}

export function getQuery(server, query :String) {
  return new Promise((resolve, reject) => {
    let data = "";
    const req = http.request({
      hostname: server.hostname,
      port: server.port,
      path: '/graphql',
      method: 'POST',
      headers: {
        'Content-Type': 'application/graphql'
      }
    }, res => {
      res.setEncoding('utf8');
      res.on('data', chunk => {
        data += chunk;
      });
      res.on('end', () => {
        resolve(JSON.parse(data));
      });
    });

    req.on('error', (e) => {
      reject(e);
    });
    req.write(query);
    req.end();
  });
}
