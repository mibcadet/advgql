import { Connection, createConnection, Model, Collection, Document, Schema } from 'mongoose';
import { IUserModel, userSchema } from '../models/User';

export class MongoDB {
  connection: Connection;
  user: Model<IUserModel>;

  constructor(dbUrl :string) {
    this.connection = createConnection(dbUrl);
    this.user = this.connection.model<IUserModel>('User', userSchema);
  }
};
