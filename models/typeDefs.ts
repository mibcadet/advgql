const types = `
  type Token {
    _id: String!
    hash: String!
    createdAt: String!
  }

  type User {
    id: Float!
    username: String!
    password: String!
    token: Token
    email: String
  }

  type Query {
    users(hash: Float): [User]
  }

  type Mutation {
    getToken(username: String, password: String) :Token
  }
`;

export const typeDefs = types;
